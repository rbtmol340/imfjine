<?php

namespace App\Filament\Resources\TabArchiveResource\Pages;

use App\Filament\Resources\TabArchiveResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTabArchive extends CreateRecord
{
    protected static string $resource = TabArchiveResource::class;
}
