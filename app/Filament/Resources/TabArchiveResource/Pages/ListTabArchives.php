<?php

namespace App\Filament\Resources\TabArchiveResource\Pages;

use App\Filament\Resources\TabArchiveResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListTabArchives extends ListRecords
{
    protected static string $resource = TabArchiveResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
