<?php

namespace App\Filament\Resources\TabArchiveResource\Pages;

use App\Filament\Resources\TabArchiveResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditTabArchive extends EditRecord
{
    protected static string $resource = TabArchiveResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
