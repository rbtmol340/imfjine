<?php

namespace App\Filament\Resources\RecommendedResource\Pages;

use App\Filament\Resources\RecommendedResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateRecommended extends CreateRecord
{
    protected static string $resource = RecommendedResource::class;
}
