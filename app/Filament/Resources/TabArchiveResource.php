<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TabArchiveResource\Pages;
use App\Filament\Resources\TabArchiveResource\RelationManagers;
use App\Models\TabArchive;
use Filament\Forms;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class TabArchiveResource extends Resource
{
    protected static ?string $model = TabArchive::class;
    protected static ?string $navigationGroup = 'Archives';
    protected static ?int $navigationSort = 1;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('name')
                    ->required(),
                  TextInput::make('link')
                      ->required()
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')->searchable(),
                TextColumn::make('link')->searchable()->color('primary'),
                TextColumn::make('created_at')->searchable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ])
            ->emptyStateActions([
                Tables\Actions\CreateAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTabArchives::route('/'),
//            'create' => Pages\CreateTabArchive::route('/create'),
//            'edit' => Pages\EditTabArchive::route('/{record}/edit'),
        ];
    }
}
