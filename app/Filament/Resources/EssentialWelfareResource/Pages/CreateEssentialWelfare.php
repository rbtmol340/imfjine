<?php

namespace App\Filament\Resources\EssentialWelfareResource\Pages;

use App\Filament\Resources\EssentialWelfareResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateEssentialWelfare extends CreateRecord
{
    protected static string $resource = EssentialWelfareResource::class;
}
