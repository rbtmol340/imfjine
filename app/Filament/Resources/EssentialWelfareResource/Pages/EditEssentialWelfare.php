<?php

namespace App\Filament\Resources\EssentialWelfareResource\Pages;

use App\Filament\Resources\EssentialWelfareResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditEssentialWelfare extends EditRecord
{
    protected static string $resource = EssentialWelfareResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
